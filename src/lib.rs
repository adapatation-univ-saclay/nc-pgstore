use pyo3::prelude::*;

use chrono::{NaiveDate, NaiveDateTime};
use itertools::{iproduct, Itertools};
use postgis::ewkb::Point;
use postgres::types::ToSql;
use postgres::{Client, Error as PGError, NoTls};
use std::error::Error;
use std::time::Duration;

fn parse_initial_date(init_date: &str) -> NaiveDateTime {
    let ds = "days since ";
    let date_str: String = init_date.chars().skip(ds.len()).collect();
    let date = NaiveDateTime::parse_from_str(&date_str, "%Y-%m-%d %H:%M:%S")
        .and_then(|dt| Ok(dt.date()))
        .or_else(|_err| NaiveDate::parse_from_str(&date_str, "%Y-%m-%d"))
        .unwrap();

    let dt = date.and_hms_opt(0, 0, 0).unwrap();

    return dt;
}

fn compute_date(init_date: NaiveDateTime, delta: f32) -> NaiveDateTime {
    let delta_duration = Duration::from_secs_f32(delta * 86400.0);
    init_date + delta_duration
}

fn store_netcdf_file(
    file_path: &str,
    batch_size: usize,
    client: &mut Client,
    schema: &str,
    table: &str,
    indicator_id: i32,
    scenario_id: i32,
    model_id: i32,
) -> Result<(), Box<dyn Error>> {
    let file = netcdf::open(file_path)?;

    let vid = file
        .attribute("variable_id")
        .ok_or("Could not find variable name in NetCDF file")?;
    let vname: String = vid.value()?.try_into()?;

    let time_variable = file
        .variable("time")
        .ok_or("Could not find time variable in NetCDF file")?;
    let initial_date_attr = time_variable
        .attribute("units")
        .ok_or("Could not find time unit in time variable in NetCDF file")?;

    let init_date_str: String = initial_date_attr.value()?.try_into()?;
    let init_date = parse_initial_date(&init_date_str);

    let time_dim = file
        .dimension("time")
        .ok_or("Could not find time dimension in NetCDF file")?;

    let lat_dim = file
        .dimension("lat")
        .ok_or("Could not find latitude ('lat') dimension in NetCDF file")?;

    let lon_dim = file
        .dimension("lon")
        .ok_or("Could not find longitude ('lon') dimension in NetCDF file")?;

    let var = &file
        .variable(&vname)
        .expect("Could not find provided variable in NetCDF file");

    let times = &file
        .variable("time")
        .expect("Could not find time values in NetCDF file");

    let lats = &file
        .variable("lat")
        .expect("Could not find latitude (lat) values in NetCDF file");

    let lons = &file
        .variable("lon")
        .expect("Could not find longitude (lon) values in NetCDF file");

    //TODO: load in batches if file size is too large
    let data = var.get::<f64, _>(..)?;
    let time_deltas = times.get::<f32, _>(..)?;
    let latitudes = lats.get::<f64, _>(..)?;
    let longitudes = lons.get::<f64, _>(..)?;

    let mut accumulator = vec![];

    let multi_index_iterator = iproduct!(0..time_dim.len(), 0..lat_dim.len(), 0..lon_dim.len());

    for (iteration, (t, lat_idx, lon_idx)) in multi_index_iterator.enumerate() {
        let val = data[[t, lat_idx, lon_idx]];
        let date = compute_date(init_date, time_deltas[t]);
        let point = Point {
            y: latitudes[lat_idx],
            x: longitudes[lon_idx],
            srid: Some(4326),
        };
        accumulator.push((point, date, val));
        if (iteration + 1) % batch_size == 0 && iteration != 0 {
            pg_insert_batch(
                client,
                schema,
                table,
                indicator_id,
                scenario_id,
                model_id,
                &accumulator,
            )?;
            accumulator.clear();
        }
    }

    if accumulator.len() != 0 {
        pg_insert_batch(
            client,
            schema,
            table,
            indicator_id,
            scenario_id,
            model_id,
            &accumulator,
        )?;
    }

    return Ok(());
}

fn pg_connect(connection_args: PostgresConnectionArgs) -> Result<Client, PGError> {
    let pg_connection_string = format!(
        "postgresql://{}:{}@{}:{}/{}",
        connection_args.username,
        connection_args.password,
        connection_args.host,
        connection_args.port,
        connection_args.database
    );
    let client = Client::connect(&pg_connection_string, NoTls)?;

    Ok(client)
}

fn pg_insert_batch(
    client: &mut Client,
    schema: &str,
    table: &str,
    indicator_id: i32,
    scenario_id: i32,
    model_id: i32,
    values: &Vec<(Point, NaiveDateTime, f64)>,
) -> Result<(), PGError> {
    //indicator_id, scenario_id, model_id

    let query = format!(
        "INSERT INTO {}.{}(indicator_id, scenario_id, model_id, geom, timestp, val) VALUES",
        schema, table
    );

    let param_num: usize = 6;
    let placeholders_str = (0..values.len())
        .map(|row| {
            let placeholders_tuple = ((1 + row * param_num)..(1 + (row + 1) * param_num))
                .map(|i| format!("${}", i))
                .join(",");
            format!("({})", placeholders_tuple)
        })
        .join(",");

    let params: Vec<&(dyn ToSql + Sync)> = values
        .iter()
        .map(|(point, date, value)| {
            let v: [&(dyn ToSql + Sync); 6] =
                [&indicator_id, &scenario_id, &model_id, point, date, value];
            v
        })
        .flatten()
        .collect();

    client.execute(&format!("{} {};", query, placeholders_str), &params)?;

    Ok(())
}

#[derive(FromPyObject, Clone)]
struct PostgresConnectionArgs {
    username: String,
    password: String,
    host: String,
    database: String,
    port: i32,
}

//Stores a NetCDF file in the indicator values table
#[pyfunction]
fn store_netcdf_file_to_postgis(
    file_path: &str,
    connection_args: PostgresConnectionArgs,
    batch_size: usize,
    schema: &str,
    table: &str,
    indicator_id: i32,
    scenario_id: i32,
    model_id: i32,
) {
    let mut client = pg_connect(connection_args).expect("Could not connect to postgres database.");

    store_netcdf_file(
        &file_path,
        batch_size,
        &mut client,
        &schema,
        &table,
        indicator_id,
        scenario_id,
        model_id,
    )
    .expect("Could not store NetCDF file in the database.");
}

/// A Python module implemented in Rust to store NetCDF files in the database.
#[pymodule]
fn nc_pgstore(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(store_netcdf_file_to_postgis, m)?)?;
    Ok(())
}
